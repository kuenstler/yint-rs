use crossterm::event::{KeyEvent, MouseEvent};

pub enum PipeCommands {
    Key(KeyEvent),
    Resize,
    Mouse(MouseEvent),
}
