use std::io::Stdout;
use tui::{
    backend::CrosstermBackend,
    layout::Rect,
    style::{Modifier, Style},
    text::{Span, Spans},
    widgets::{Block, Borders, List, ListItem},
    Frame,
};

use crate::ui::utils::App;

pub fn draw_first_tab(f: &mut Frame<CrosstermBackend<Stdout>>, app: &mut App, area: Rect) {
    draw_charts(f, app, area);
}

fn draw_charts(f: &mut Frame<CrosstermBackend<Stdout>>, app: &mut App, area: Rect) {
    // Draw tasks
    let tasks: Vec<ListItem> = app
        .tasks
        .items
        .iter()
        .map(|i| ListItem::new(vec![Spans::from(Span::raw(*i))]))
        .collect();
    let tasks = List::new(tasks)
        .block(Block::default().borders(Borders::ALL).title("List"))
        .highlight_style(Style::default().add_modifier(Modifier::BOLD))
        .highlight_symbol("> ");
    f.render_stateful_widget(tasks, area, &mut app.tasks.state);
}

pub fn on_key(app: &mut App, c: char) {}
