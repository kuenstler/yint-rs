use std::{
    error::Error,
    io::{stdout, Write},
    sync::mpsc,
    thread,
};

use crossterm::{
    event::{self, DisableMouseCapture, EnableMouseCapture, Event},
    execute,
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
};
use tui::{backend::CrosstermBackend, Terminal};

use crossterm::event::KeyCode;
use widgets::SuperWidget;

mod tabs;
/// Collection of Widgets using the SuperWidget trait
pub mod widgets;

mod pipe;

use pipe::PipeCommands;

pub fn main() -> Result<(), Box<dyn Error>> {
    enable_raw_mode()?;

    let mut stdout = stdout();
    execute!(stdout, EnterAlternateScreen, EnableMouseCapture)?;

    let backend = CrosstermBackend::new(stdout);

    let mut terminal = Terminal::new(backend)?;

    // Setup input handling
    let (tx, rx) = mpsc::channel();

    thread::spawn(move || loop {
        tx.send(match event::read().unwrap() {
            Event::Key(key) => PipeCommands::Key(key),
            Event::Mouse(mouse) => PipeCommands::Mouse(mouse),
            Event::Resize(..) => PipeCommands::Resize,
        })
        .unwrap();
    });

    let mut tab = tabs::get_tabs();
    let mut should_quit = false;

    terminal.clear()?;

    loop {
        terminal.draw(|f| tab.render(f, f.size()))?;
        terminal.show_cursor()?;
        let position = tab.cursor_position(terminal.size()?);
        terminal.set_cursor(position.x, position.y)?;

        match rx.recv()? {
            PipeCommands::Key(key) => {
                let key = tab.keypress(key);

                if let Some(k) = key {
                    if let KeyCode::Char('q') = k.code {
                        disable_raw_mode()?;
                        execute!(
                            terminal.backend_mut(),
                            LeaveAlternateScreen,
                            DisableMouseCapture
                        )?;
                        terminal.show_cursor()?;
                        should_quit = true;
                    }
                }
            }
            PipeCommands::Resize => {}
            PipeCommands::Mouse(_) => {}
        }
        if should_quit {
            break;
        }
    }

    Ok(())
}
