use super::widgets::*;
use tui::layout::Constraint;
use tui::style::{Color, Style};

pub fn get_tabs() -> SuperTab {
    let some_gauge1 = SuperGauge::new()
        .move_set_tag("Gauge 1".to_string())
        .move_set_ratio(0.5)
        .unwrap();
    let some_paragraph = SuperParagraph::new("Endless Text".to_string());
    let some = SuperLayout::new()
        .move_set_members_constraints(
            vec![Box::from(some_gauge1), Box::from(some_paragraph)],
            vec![Constraint::Length(2), Constraint::Min(2)],
        )
        .unwrap()
        .move_set_focus(1)
        .unwrap()
        .move_set_topmost();
    let thing = SuperButton::new("ID".to_string(), "A Button\nWith a line break".to_string())
        .move_add_action('c', &|_, _, _| println!("Test_c"), true);
    let overlay = SuperOverlay::new(Box::from(SuperEmpty::new()));
    SuperTab::new()
        .move_set_title_and_content(
            vec![
                "Some".to_string(),
                "title".to_string(),
                "Overlay".to_string(),
            ],
            vec![Box::from(some), Box::from(thing), Box::from(overlay)],
        )
        .unwrap()
        .move_set_focus(1)
        .unwrap()
        .move_set_style(Style::default().bg(Color::Rgb(0, 0, 0)).fg(Color::Blue))
        .move_set_highlight_style(Style::default().fg(Color::Green).bg(Color::Red))
        .move_set_topmost()
}
