use std::{char, collections::HashMap, io::Stdout};

use async_std::task;
use crossterm::event::{KeyCode, KeyEvent};
use sqlx::{sqlite::SqliteConnection, Connect};
use tui::{
    backend::CrosstermBackend,
    buffer::Buffer,
    layout::{Constraint, Direction, Layout, Rect},
    style::Style,
    widgets::{Paragraph, Widget, Wrap},
    Frame,
};

use crate::ui::widgets::SuperWidget;

pub struct SuperButton<'a> {
    actions: HashMap<char, &'a dyn Fn(&str, char, SqliteConnection) -> ()>,
    default_action: char,
    pub id: String,
    pub text: String,
    pub style: Style,
}

impl<'a> SuperButton<'a> {
    pub fn new(id: String, text: String) -> SuperButton<'a> {
        SuperButton {
            actions: HashMap::new(),
            default_action: '\0',
            id,
            text,
            style: Style::default(),
        }
    }

    pub fn move_set_text(mut self, text: String) -> SuperButton<'a> {
        self.text = text;
        self
    }

    pub fn move_set_style(mut self, style: Style) -> SuperButton<'a> {
        self.style = style;
        self
    }

    pub fn add_action(
        &mut self,
        key: char,
        value: &'a dyn Fn(&str, char, SqliteConnection) -> (),
        set_as_default: bool,
    ) {
        if set_as_default {
            self.default_action = key;
        }
        self.actions.insert(key, value);
    }

    pub fn move_add_action(
        mut self,
        key: char,
        value: &'a dyn Fn(&str, char, SqliteConnection) -> (),
        set_as_default: bool,
    ) -> SuperButton<'a> {
        self.add_action(key, value, set_as_default);
        self
    }
}

impl<'a> SuperWidget for SuperButton<'a> {
    fn render(&mut self, frame: &mut Frame<CrosstermBackend<Stdout>>, area: Rect) {
        if area.width < 6 {
            return;
        };
        let layout = Layout::default()
            .constraints(
                vec![
                    Constraint::Length(2),
                    Constraint::Min(2),
                    Constraint::Length(2),
                ]
                .as_ref(),
            )
            .direction(Direction::Horizontal)
            .split(area);
        let paragraph = Paragraph::new(&self.text[..])
            .style(self.style)
            .wrap(Wrap { trim: true });
        let len = {
            let orig_buf = Buffer::empty(layout[1].clone());
            let mut buf = orig_buf.clone();
            paragraph.clone().render(layout[1].clone(), &mut buf);
            if let Some(pos) = buf.diff(&orig_buf).last() {
                pos.1
            } else {
                0
            }
        };
        let left_right = if len <= 0 {
            vec!["[ ".to_string(), " ]".to_string()]
        } else {
            let modifiers: Vec<u32> = (0..=len)
                .map(|pos| {
                    if pos == 0 {
                        0
                    } else if pos < len {
                        1
                    } else {
                        2
                    }
                })
                .collect();
            [0x23a1, 0x23a4]
                .iter()
                .map(|base| {
                    modifiers
                        .iter()
                        .map(|modifier| format!("{} \n", char::from_u32(base + modifier).unwrap()))
                        .collect()
                })
                .collect()
        };
        frame.render_widget(
            Paragraph::new(&left_right[0][..]).style(self.style),
            layout[0],
        );
        frame.render_widget(paragraph, layout[1]);
        frame.render_widget(
            Paragraph::new(&left_right[1][..]).style(self.style),
            layout[2],
        );
    }

    fn cursor_position(&self, area: Rect) -> Rect {
        let mut pos = area;
        pos.x += 2;
        pos
    }

    fn selected(&self) -> &dyn SuperWidget {
        self
    }

    fn mut_selected(&mut self) -> &mut dyn SuperWidget {
        self
    }

    fn keypress(&mut self, keypress: KeyEvent) -> Option<KeyEvent> {
        // TODO: Add real db handling
        let conn = task::block_on(SqliteConnection::connect("sqlite::memory:")).unwrap();
        match keypress.code {
            KeyCode::Char(c) => {
                if let Some(closure) = self.actions.get(&c) {
                    closure(&self.id, c, conn);
                    None
                } else {
                    Some(keypress)
                }
            }
            KeyCode::Enter => {
                if let Some(closure) = self.actions.get(&self.default_action) {
                    closure(&self.id, self.default_action, conn);
                    None
                } else {
                    Some(keypress)
                }
            }
            _ => Some(keypress),
        }
    }

    fn selectable(&self) -> bool {
        true
    }
}
