use unicode_segmentation::Graphemes;
use unicode_width::UnicodeWidthStr;

const NBSP: &str = "\u{00a0}";

/// Stolen from tui/widgets/reflow.rs
pub struct WordWrapper<'a> {
    symbols: &'a mut Graphemes<'a>,
    max_line_width: u16,
    current_line: Vec<&'a str>,
    next_line: Vec<&'a str>,
    /// Removes the leading whitespace from lines
    trim: bool,
    strip_whitespace: bool,
}

impl<'a> WordWrapper<'a> {
    pub fn new(
        symbols: &'a mut Graphemes<'a>,
        max_line_width: u16,
        trim: bool,
        strip_whitespace: bool,
    ) -> WordWrapper<'a> {
        WordWrapper {
            symbols,
            max_line_width,
            current_line: vec![],
            next_line: vec![],
            trim,
            strip_whitespace,
        }
    }

    pub fn next_line(&mut self) -> Option<(&[&'a str], u16)> {
        if self.max_line_width == 0 {
            return None;
        }
        std::mem::swap(&mut self.current_line, &mut self.next_line);
        self.next_line.truncate(0);

        let mut current_line_width = self
            .current_line
            .iter()
            .map(|symbol| symbol.width() as u16)
            .sum();

        let mut symbols_to_last_word_end: usize = 0;
        let mut width_to_last_word_end: u16 = 0;
        let mut prev_whitespace = false;
        let mut symbols_exhausted = true;
        for symbol in &mut self.symbols {
            symbols_exhausted = false;
            let symbol_whitespace = symbol.chars().all(&char::is_whitespace);

            // Ignore characters wider that the total max width.
            if symbol.width() as u16 > self.max_line_width
                // Skip leading whitespace when trim is enabled.
                || self.trim && symbol_whitespace && symbol != "\n" && current_line_width == 0
            {
                continue;
            }

            // Break on newline and discard it.
            if symbol == "\n" {
                if prev_whitespace {
                    current_line_width = width_to_last_word_end;
                    self.current_line.truncate(symbols_to_last_word_end);
                }
                break;
            }

            // Mark the previous symbol as word end.
            if symbol_whitespace && !prev_whitespace && symbol != NBSP {
                symbols_to_last_word_end = self.current_line.len() + 1;
                width_to_last_word_end = current_line_width + 1;
            }

            self.current_line.push(symbol);
            current_line_width += symbol.width() as u16;

            if current_line_width > self.max_line_width {
                // If there was no word break in the text, wrap at the end of the line.
                let (truncate_at, truncated_width) = if symbols_to_last_word_end != 0 {
                    (symbols_to_last_word_end, width_to_last_word_end)
                } else {
                    (self.current_line.len() - 1, self.max_line_width)
                };

                // Push the remainder to the next line but strip leading whitespace:
                {
                    let remainder = &self.current_line[truncate_at..];
                    if self.strip_whitespace {
                        if let Some(remainder_nonwhite) = remainder
                            .iter()
                            .position(|symbol| !symbol.chars().all(&char::is_whitespace))
                        {
                            self.next_line
                                .extend_from_slice(&remainder[remainder_nonwhite..]);
                        }
                    } else {
                        self.next_line.extend_from_slice(&remainder);
                    }
                }
                self.current_line.truncate(truncate_at);
                current_line_width = truncated_width;
                break;
            }

            prev_whitespace = symbol_whitespace;
        }

        // Even if the iterator is exhausted, pass the previous remainder.
        if symbols_exhausted && self.current_line.is_empty() {
            None
        } else {
            Some((&self.current_line[..], current_line_width))
        }
    }
}
