use std::io::Stdout;

use crossterm::event::{KeyCode, KeyEvent};
use tui::{
    backend::CrosstermBackend,
    layout::{Constraint, Direction, Layout, Rect},
    style::Style,
    text::{Span, Spans},
    widgets::Tabs,
    Frame,
};

use crate::ui::widgets::{Error, SuperWidget};

pub struct SuperTab {
    titles: Vec<String>,
    content: Vec<Box<dyn SuperWidget>>,
    pub style: Style,
    pub highlight_style: Style,
    focus: usize,
    pub topmost: bool,
}

impl SuperTab {
    pub fn new() -> SuperTab {
        SuperTab {
            titles: vec![],
            content: vec![],
            style: Default::default(),
            highlight_style: Default::default(),
            focus: 0,
            topmost: false,
        }
    }

    /// Set the titles and contents of tabs. The numbers of both need to match.
    /// Returns Error otherwise.
    ///
    /// # Examples
    ///
    /// ```
    /// use crate::ui::widgets::SuperTab;
    /// let content = vec![SuperTab::new(), SuperTab::new()];
    /// let titles = vec!["Tab1".to_string(), "Tab2".to_string()];
    /// let mut super_tab = SuperTab::new();
    /// super_tab.set_title_and_content(titles.clone(), content.clone()).unwrap();
    ///
    /// assert_eq!(super_tab.titles(), titles);
    /// assert_eq!(super_tab.content(), content);
    /// ```
    pub fn set_title_and_content(
        &mut self,
        titles: Vec<String>,
        contents: Vec<Box<dyn SuperWidget>>,
    ) -> Result<(), Error> {
        if titles.len() != contents.len() {
            return Err(Error {
                message: "set title and content has different length".to_string(),
            });
        }
        let mut titles_len = titles.len();
        if titles_len > 0 {
            titles_len -= 1;
        };
        if titles_len > self.focus {
            self.focus = titles_len
        }
        self.titles = titles;
        self.content = contents;
        Ok(())
    }

    /// Set the titles and contents of tabs. The numbers of both need to match.
    /// Returns Error otherwise and SuperTab upon success.
    ///
    /// # Examples
    ///
    /// ```
    /// use crate::ui::widgets::SuperTab;
    /// let content = vec![SuperTab::new(), SuperTab::new()];
    /// let titles = vec!["Tab1".to_string(), "Tab2".to_string()];
    /// let super_tab = SuperTab::new()
    ///     .move_set_title_and_content(titles.clone(), content.clone()).unwrap();
    ///
    /// assert_eq!(super_tab.titles(), titles);
    /// assert_eq!(super_tab.content(), content);
    /// ```
    pub fn move_set_title_and_content(
        mut self,
        titles: Vec<String>,
        contents: Vec<Box<dyn SuperWidget>>,
    ) -> Result<SuperTab, Error> {
        self.set_title_and_content(titles, contents)?;
        Ok(self)
    }

    pub fn titles(&self) -> &Vec<String> {
        &self.titles
    }

    pub fn content(&self) -> &Vec<Box<dyn SuperWidget>> {
        &self.content
    }

    pub fn move_set_style(mut self, style: Style) -> SuperTab {
        self.style = style;
        self
    }

    pub fn move_set_highlight_style(mut self, style: Style) -> SuperTab {
        self.highlight_style = style;
        self
    }

    /// Set the focused tab. Returns Error if the focus is beyond the last title.
    ///
    /// # Examples
    ///
    /// ```
    /// use crate::ui::widgets::SuperTab;
    /// let content = vec![SuperTab::new(), SuperTab::new()];
    /// let titles = vec!["Tab1".to_string(), "Tab2".to_string()];
    /// let mut super_tab = SuperTab::new()
    ///     .move_set_title_and_content(titles.clone(), content.clone()).unwrap();
    /// super_tab.set_focus(1);
    ///
    /// assert_eq!(super_tab.focus(), 1);
    /// ```
    pub fn set_focus(&mut self, focus: usize) -> Result<(), Error> {
        if focus > self.titles.len() - 1 {
            return Err(Error {
                message: "Focus set behind last item of SuperTab".to_string(),
            });
        }
        self.focus = focus;
        Ok(())
    }

    /// Set the focused tab. Returns Error if the focus is beyond the last title.
    /// Returns the SuperTab itself otherwise.
    ///
    /// # Examples
    ///
    /// ```
    /// use crate::ui::widgets::SuperTab;
    /// let content = vec![SuperTab::new(), SuperTab::new()];
    /// let titles = vec!["Tab1".to_string(), "Tab2".to_string()];
    /// let mut super_tab = SuperTab::new()
    ///     .move_set_title_and_content(titles.clone(), content.clone()).unwrap()
    ///     .move_set_focus(1);
    ///
    /// assert_eq!(super_tab.focus(), 1);
    /// ```
    pub fn move_set_focus(mut self, focus: usize) -> Result<SuperTab, Error> {
        self.set_focus(focus)?;
        Ok(self)
    }

    pub fn move_set_topmost(mut self) -> SuperTab {
        self.topmost = true;
        self
    }

    /// Move the pointer 1 right, moves to the first item if the last item is selected
    ///
    /// ```
    /// use crate::ui::widgets::SuperTab;
    /// let mut super_tab: SuperTab = SuperTab::new();
    /// super_tab.move_right();
    /// ```
    pub fn move_right(&mut self) {
        if self.titles.len() >= 1 {
            if self.focus < self.titles.len() - 1 {
                self.focus += 1;
            } else if self.topmost {
                self.focus = 0;
            }
        };
    }

    fn move_right_none(&mut self, keypress: KeyEvent) -> Option<KeyEvent> {
        let before = self.focus;
        self.move_right();
        if self.focus != before {
            None
        } else {
            Some(keypress)
        }
    }

    /// Move the pointer 1 left, moves to the first item if the last item is selected
    ///
    /// ```
    /// use crate::ui::widgets::SuperTab;
    /// let mut super_tab: SuperTab = SuperTab::new();
    /// super_tab.move_left();
    /// ```
    pub fn move_left(&mut self) {
        if self.titles.len() >= 1 {
            if self.focus > 0 {
                self.focus -= 1;
            } else if self.topmost {
                self.focus = self.titles.len() - 1;
            };
        };
    }

    fn move_left_none(&mut self, keypress: KeyEvent) -> Option<KeyEvent> {
        let before = self.focus;
        self.move_left();
        if self.focus != before {
            None
        } else {
            Some(keypress)
        }
    }

    fn try_process_fkey(&mut self, fkey: KeyEvent) -> Option<KeyEvent> {
        if let KeyCode::F(i) = fkey.code {
            if self.topmost {
                let j = (i - 1) as usize;
                if j <= self.titles.len() {
                    self.focus = j;
                    return None;
                }
            }
        }
        Some(fkey)
    }

    fn get_chunks(area: Rect) -> Vec<Rect> {
        Layout::default()
            .constraints([Constraint::Length(1), Constraint::Min(0)].as_ref())
            .direction(Direction::Vertical)
            .split(area)
    }
}

impl SuperWidget for SuperTab {
    fn render(&mut self, frame: &mut Frame<CrosstermBackend<Stdout>>, area: Rect) {
        let chunks = SuperTab::get_chunks(area);
        let titles = self
            .titles
            .iter()
            .map(|t| Spans::from(Span::styled(t, self.style.clone())))
            .collect();
        let tabs = Tabs::new(titles)
            .highlight_style(self.highlight_style.clone())
            .select(self.focus);
        frame.render_widget(tabs, chunks[0]);
        if self.content.len() > 0 {
            self.content[self.focus].render(frame, chunks[1]);
        }
    }

    fn cursor_position(&self, area: Rect) -> Rect {
        let chunks = SuperTab::get_chunks(area);
        if self.content.len() > 0 {
            self.content[self.focus].cursor_position(chunks[1])
        } else {
            chunks[0]
        }
    }

    fn selected(&self) -> &dyn SuperWidget {
        self.content[self.focus].as_ref()
    }

    fn mut_selected(&mut self) -> &mut dyn SuperWidget {
        self.content[self.focus].as_mut()
    }

    fn keypress(&mut self, keypress: KeyEvent) -> Option<KeyEvent> {
        let new_keypress = self.mut_selected().keypress(keypress)?;
        match new_keypress.code {
            KeyCode::Right => self.move_right_none(keypress),
            KeyCode::Left => self.move_left_none(keypress),
            KeyCode::F(_) => self.try_process_fkey(new_keypress),
            _ => Some(new_keypress),
        }
    }

    fn selectable(&self) -> bool {
        true
    }
}
