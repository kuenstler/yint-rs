use std::io::Stdout;

use crossterm::event::KeyEvent;
use tui::{backend::CrosstermBackend, layout::Rect, Frame};

use crate::ui::widgets::SuperWidget;

pub struct SuperEmpty;

impl SuperEmpty {
    pub fn new() -> SuperEmpty {
        SuperEmpty
    }
}

impl SuperWidget for SuperEmpty {
    fn render(&mut self, _frame: &mut Frame<CrosstermBackend<Stdout>>, _area: Rect) {}

    fn cursor_position(&self, area: Rect) -> Rect {
        area
    }

    fn selected(&self) -> &dyn SuperWidget {
        self
    }

    fn mut_selected(&mut self) -> &mut dyn SuperWidget {
        self
    }

    fn keypress(&mut self, keypress: KeyEvent) -> Option<KeyEvent> {
        Some(keypress)
    }

    fn selectable(&self) -> bool {
        false
    }
}
