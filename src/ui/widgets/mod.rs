#![allow(dead_code)]
use std::fmt;
use std::io::Stdout;

use crossterm::event::KeyEvent;
use tui::{backend::CrosstermBackend, layout::Rect, Frame};

pub use button::SuperButton;
pub use edit::SuperEdit;
pub use empty::SuperEmpty;
pub use gauge::SuperGauge;
pub use layout::SuperLayout;
pub use overlay::SuperOverlay;
pub use paragraph::SuperParagraph;
pub use tab::SuperTab;

mod button;
mod edit;
mod empty;
mod gauge;
mod layout;
mod overlay;
mod paragraph;
mod tab;
mod word_wrapper;

/// A special widget type, which won't get consumed while rendering and should be reusable.
pub trait SuperWidget {
    /// Render a super Widget, doesn't consume the SuperWidget
    fn render(&mut self, frame: &mut Frame<CrosstermBackend<Stdout>>, area: Rect);
    /// Get the cursor position, called from the topmost widget, width and height are ignored
    fn cursor_position(&self, area: Rect) -> Rect;
    /// Returns the currently selected SuperWidget
    fn selected(&self) -> &dyn SuperWidget;
    /// Returns a mutable reference to the currently selected SuperWidget
    fn mut_selected(&mut self) -> &mut dyn SuperWidget;
    /// Processes a keypress, returns Some(keypress) if nothing consumed the keypress
    /// and None if the keypress got consumed
    fn keypress(&mut self, keypress: KeyEvent) -> Option<KeyEvent>;
    /// Returns if the widget is selectable
    fn selectable(&self) -> bool;
}

#[derive(Debug)]
pub struct Error {
    pub message: String,
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.message)
    }
}

impl std::error::Error for Error {}
