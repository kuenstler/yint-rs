use std::cmp;
use std::io::Stdout;

use crossterm::event::{KeyCode, KeyEvent, KeyModifiers};
use tui::{
    backend::CrosstermBackend,
    layout::Rect,
    style::Style,
    text::Text,
    widgets::{Paragraph, Wrap},
    Frame,
};
use unicode_segmentation::{Graphemes, UnicodeSegmentation};

use crate::ui::widgets::{word_wrapper::WordWrapper, SuperWidget};

pub struct SuperEdit {
    pub prompt: String,
    pub text: String,
    pub multiline: bool,
    pub style: Style,
    x: u16,
    last_area: Rect,
}

impl SuperEdit {
    pub fn new() -> SuperEdit {
        SuperEdit {
            prompt: "".to_string(),
            text: "".to_string(),
            multiline: false,
            style: Style::default(),
            x: 0,
            last_area: Rect {
                x: 0,
                y: 0,
                width: 0,
                height: 0,
            },
        }
    }

    pub fn move_set_prompt(mut self, prompt: String) -> SuperEdit {
        self.prompt = prompt;
        self
    }

    pub fn move_set_text(mut self, text: String) -> SuperEdit {
        self.text = text;
        self
    }

    pub fn move_set_multiline(mut self) -> SuperEdit {
        self.multiline = true;
        self
    }

    pub fn move_set_style(mut self, style: Style) -> SuperEdit {
        self.style = style;
        self
    }

    fn get_text(&self) -> String {
        format!("{}{}", &self.prompt, &self.text)
    }

    fn get_lines_from_graphemes<'a>(
        &self,
        graphemes: &'a mut Graphemes<'a>,
        extend_whitespace: bool,
    ) -> Vec<Vec<String>> {
        let mut word = WordWrapper::new(graphemes, self.last_area.width, true, extend_whitespace);
        let mut lines: Vec<Vec<String>> = Vec::new();
        while let Some((current_line, _)) = word.next_line() {
            lines.push(current_line.iter().map(|i| i.to_string()).collect());
        }
        lines
    }

    fn get_text_to_cursor(&self) -> &str {
        &self.text[..self.x as usize]
    }

    fn get_lines_to_cursor(
        &self,
        include_prompt: bool,
        extend_whitespace: bool,
    ) -> Vec<Vec<String>> {
        let text = self.get_text();
        let mut graphemes = if include_prompt {
            text[..self.prompt.len() + self.x as usize].graphemes(true)
        } else {
            self.get_text_to_cursor().graphemes(true)
        };
        self.get_lines_from_graphemes(&mut graphemes, extend_whitespace)
    }

    fn get_text_from_cursor(&self) -> &str {
        &self.text[self.x as usize..]
    }

    fn get_lines_from_cursor(&self, extend_whitespace: bool) -> Vec<Vec<String>> {
        let mut graphemes = self.get_text_from_cursor().graphemes(true);
        self.get_lines_from_graphemes(&mut graphemes, extend_whitespace)
    }

    fn get_cursor_position(&self, include_prompt: bool, shrink_to_area: bool) -> (u16, u16) {
        let lines = self.get_lines_to_cursor(include_prompt, true);
        let mut y = lines.len() - 1;
        let mut x = if let Some(last) = lines.last() {
            last.len()
        } else {
            0
        };
        if x as u16 >= self.last_area.width && shrink_to_area {
            x = 0;
            y += 1;
        }
        (x as u16, y as u16)
    }

    fn handle_backspace(&mut self, keypress: KeyEvent) -> Option<KeyEvent> {
        if keypress.modifiers == KeyModifiers::CONTROL {
            let mut word = self.get_text_to_cursor().split_word_bound_indices();
            if let Some((from, _)) = word.next_back() {
                self.text.drain(from..self.x as usize);
                self.x = from as u16;
            } else {
                return Some(keypress);
            }
        } else {
            let mut graphmeme = self.get_text_to_cursor().grapheme_indices(true);
            if let Some((from, _)) = graphmeme.next_back() {
                self.text.drain(from..self.x as usize);
                self.x = from as u16;
            } else {
                return Some(keypress);
            }
        }
        None
    }

    fn handle_up(&mut self, keypress: KeyEvent) -> Option<KeyEvent> {
        // No text -> no action
        if self.text.len() > 0 {
            // Text for validating the new position
            let mut text = self.get_lines_to_cursor(true, true);
            // Text for looking up the difference
            let mut text_with_whitespace = self.get_lines_to_cursor(true, false);
            // Grapheme lines of prompt
            let prompt = self.get_lines_from_graphemes(&mut self.prompt.graphemes(true), true);
            // Cursor position
            let pos = self.get_cursor_position(true, false);
            // Prompt ends with line -> no modifier needed
            let modifier = if prompt.len() == 0 || text.contains(prompt.last().unwrap()) {
                0
            } else {
                1
            };
            // Self explanatory
            let first_text_line = prompt.len() - modifier;
            // len() of text lines: one line -> 1
            let text_lines = pos.1 as usize - first_text_line + 1;
            // Validate the new position using the known facts
            if text_lines > 1
                && (modifier == 0
                    || pos.0 == 0
                    || text_lines > 2
                    || pos.0 >= prompt.last().unwrap().len() as u16)
            {
                // Discard a specific edge case
                if pos.0 == 0 && modifier != 0 && text_lines == 2 {
                    return Some(keypress);
                }
                // previous line to be edited later on
                let mut new_text = text.remove(pos.1 as usize - 1);
                // len of previous line
                let before = new_text.len();
                // remove either len (all) items or until cursor pos, whatever is less
                new_text.drain(..cmp::min(pos.0 as usize, before));
                // add current line with whitespaces for len purposes
                new_text.append(&mut text_with_whitespace[pos.1 as usize]);
                // size needet to be removed
                let size = new_text.join("").len() as u16;
                // finally remove it after so much evaluation
                self.x -= size;
                // don't return a keypress
                return None;
            }
        }
        Some(keypress)
    }

    fn handle_down(&mut self, keypress: KeyEvent) -> Option<KeyEvent> {
        // No text -> no action
        if self.text.len() > 0 {
            // Text for validating the new position
            // TODO: Not aware of future characters -> no line break were a linebreak is neccesary
            let text = self.get_lines_from_cursor(true);
            // Text for looking up the difference
            let mut text_with_whitespace = self.get_lines_from_cursor(false);
            // text before cursor
            let previous_text = self.get_lines_to_cursor(true, true);
            // Cursor position
            let pos = self.get_cursor_position(true, false);
            // whether pos is the last character of the line
            let last_character = pos.0 as usize == previous_text[pos.1 as usize].len();
            // len() of text lines: one line -> 1
            let text_lines = text.len();
            // Validate the new position using the known facts
            if text_lines > 1 || text_lines == 1 && (pos.0 == 0 || last_character) {
                // len of following line
                let following_size = if text_lines > 1 || last_character {
                    let index = if last_character { 0 } else { 1 };
                    let from = cmp::min(text[index].len(), pos.0 as usize)
                        + text_with_whitespace[index].len()
                        - text[index].len();
                    // following line to be edited later on
                    let mut new_text = text_with_whitespace.remove(index);
                    new_text.drain(from..);
                    new_text.len() as u16
                } else {
                    0
                };
                // size needet to be removed
                let size = if last_character { following_size } else { following_size + text[0].len() as u16 };
                // finally add it after so much evaluation
                self.x += size;
                // don't return a keypress
                return None;
            }
        }
        Some(keypress)
    }

    fn handle_char<F>(&mut self, current: char) -> Option<F> {
        let previous_len = self.text.len() as u16;
        self.text.push(current);
        self.x += self.text.len() as u16 - previous_len;
        None
    }
}

impl SuperWidget for SuperEdit {
    fn render(&mut self, frame: &mut Frame<CrosstermBackend<Stdout>>, area: Rect) {
        self.last_area = area;
        let text = self.get_text();
        let mut paragraph = Paragraph::new(Text::from(&text[..])).wrap(Wrap { trim: true });
        let cursor_position = self.get_cursor_position(true, true);
        if cursor_position.1 > area.height {
            paragraph = paragraph.scroll((cursor_position.1 - area.height, 0));
        }
        frame.render_widget(paragraph, area);
    }

    fn cursor_position(&self, area: Rect) -> Rect {
        let mut position = area.clone();
        let potential_position = self.get_cursor_position(true, true);
        position.x = potential_position.0;
        position.y = if potential_position.1 <= area.height {
            potential_position.1
        } else {
            area.height
        };
        position.y += area.y;
        position
    }

    fn selected(&self) -> &dyn SuperWidget {
        self
    }

    fn mut_selected(&mut self) -> &mut dyn SuperWidget {
        self
    }

    fn keypress(&mut self, keypress: KeyEvent) -> Option<KeyEvent> {
        match keypress.code {
            KeyCode::Char(i) => self.handle_char(i),
            KeyCode::Backspace => self.handle_backspace(keypress),
            KeyCode::Up => self.handle_up(keypress),
            KeyCode::Down => self.handle_down(keypress),
            _ => Some(keypress),
        }
    }

    fn selectable(&self) -> bool {
        true
    }
}
