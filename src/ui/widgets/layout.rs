use std::io::Stdout;

use crossterm::event::{KeyCode, KeyEvent};
use tui::{
    backend::CrosstermBackend,
    layout::{Constraint, Direction, Layout, Rect},
    Frame,
};

use crate::ui::widgets::{Error, SuperWidget};

/// Layout with position.
/// Doesn't get consumed while rendering
///
/// # Examples
///
/// ```
/// use crate::ui::widgets::SuperLayout;
/// let mut super_layout: SuperLayout = SuperLayout::new()
///     .move_set_members(vec![SuperLayout::new()]);
/// super_layout.set_horizontal();
/// ```
pub struct SuperLayout {
    members: Vec<Box<dyn SuperWidget>>,
    constraints: Vec<Constraint>,
    selectable: Vec<usize>,
    direction: Direction,
    focus: usize,
    pub topmost: bool,
}

impl SuperLayout {
    /// Construct a new, empty SuperLayout
    pub fn new() -> SuperLayout {
        SuperLayout {
            members: Vec::new(),
            constraints: Vec::new(),
            selectable: vec![],
            direction: Direction::Vertical,
            focus: 0,
            topmost: false,
        }
    }

    /// Set the focus to a given item
    ///
    /// # Examples
    ///
    /// ```
    /// use crate::ui::widgets::SuperLayout;
    /// let mut super_layout: SuperLayout = SuperLayout::new()
    ///     .move_set_members(vec![SuperLayout::new(), SuperLayout::new()]);
    /// super_layout.set_focus(1);
    ///
    /// assert_eq!(super_layout.focus(), 1);
    /// ```
    pub fn set_focus(&mut self, focus: usize) -> Result<(), Error> {
        if focus >= self.members.len() {
            return Err(Error {
                message: "Set focus of SuperLayout to invalid position".to_string(),
            });
        }
        self.focus = focus;
        Ok(())
    }

    /// Set the focus to a given item while moving while returning SuperLayout
    ///
    /// # Examples
    ///
    /// ```
    /// use crate::ui::widgets::SuperLayout;
    /// let super_layout: SuperLayout = SuperLayout::new()
    ///     .move_set_members(vec![SuperLayout::new(), SuperLayout::new()])
    ///     .move_set_focus(1);
    ///
    /// assert_eq!(super_layout.focus(), 1);
    /// ```
    pub fn move_set_focus(mut self, focus: usize) -> Result<SuperLayout, Error> {
        self.set_focus(focus)?;
        Ok(self)
    }

    pub fn focus(&self) -> usize {
        self.focus
    }

    /// Sets the member SuperLayout's and Layout constraints.
    /// Returns Err if more members than constraints are given.
    ///
    /// # Examples
    ///
    /// ```
    /// use crate::ui::widgets::SuperLayout;
    /// use tui::layout::Constraint;
    /// let widgets = vec![SuperLayout::new(), SuperLayout::new()];
    /// let constraints = vec![Constraint::Length(1), Constraint::Min(10)];
    /// let mut super_layout: SuperLayout = SuperLayout::new();
    /// super_layout.set_members_constraints(widgets, constraints).unwrap();
    ///
    /// assert_eq!(super_layout.members(), widgets);
    /// assert_eq!(super_layout.constraints(), constraints);
    /// ```
    pub fn set_members_constraints(
        &mut self,
        members: Vec<Box<dyn SuperWidget>>,
        constraints: Vec<Constraint>,
    ) -> Result<(), Error> {
        if members.len() < constraints.len() {
            return Err(Error {
                message: "SuperLayout got less constraints than members".to_string(),
            });
        }
        let mut member_len = members.len();
        if member_len > 0 {
            member_len -= 1;
        };
        if member_len > self.focus {
            self.focus = member_len;
        }
        self.members = members;
        self.constraints = constraints;
        self.update_selectable();
        Ok(())
    }

    fn update_selectable(&mut self) {
        self.selectable = self
            .members
            .iter()
            .enumerate()
            .filter(|(_, j)| j.selectable())
            .map(|(i, _)| i)
            .collect();
    }

    /// Sets the member SuperLayout's and Layout constraints.
    /// Returns Err if more members than constraints are given or the layout itself.
    ///
    /// # Examples
    ///
    /// ```
    /// use crate::ui::widgets::SuperLayout;
    /// use tui::layout::Constraint;
    /// let widgets = vec![SuperLayout::new(), SuperLayout::new()];
    /// let constraints = vec![Constraint::Length(1), Constraint::Min(10)];
    /// let super_layout: SuperLayout = SuperLayout::new()
    ///     .move_set_members_constraints(widgets, constraints).unwrap();
    ///
    /// assert_eq!(super_layout.members(), widgets);
    /// assert_eq!(super_layout.constraints(), constraints);
    /// ```
    pub fn move_set_members_constraints(
        mut self,
        members: Vec<Box<dyn SuperWidget>>,
        constraints: Vec<Constraint>,
    ) -> Result<SuperLayout, Error> {
        self.set_members_constraints(members, constraints)?;
        Ok(self)
    }

    pub fn members(&self) -> &Vec<Box<dyn SuperWidget>> {
        &self.members
    }

    pub fn constraints(&self) -> &Vec<Constraint> {
        &self.constraints
    }

    /// Set orientation to horizontal, default is vertical
    ///
    /// ```
    /// use crate::ui::widgets::SuperLayout;
    /// let mut super_layout: SuperLayout = SuperLayout::new();
    /// super_layout.set_horizontal();
    /// ```
    pub fn set_horizontal(&mut self) {
        self.direction = Direction::Horizontal
    }

    /// Set orientation to horizontal, default is vertical, returns SuperLayout
    ///
    /// ```
    /// use crate::ui::widgets::SuperLayout;
    /// let super_layout: SuperLayout = SuperLayout::new()
    ///     .move_set_horizontal();
    /// ```
    pub fn move_set_horizontal(mut self) -> SuperLayout {
        self.set_horizontal();
        self
    }

    /// Move the pointer 1 up, moves to the last item if the first item is selected
    ///
    /// ```
    /// use crate::ui::widgets::SuperLayout;
    /// let mut super_layout: SuperLayout = SuperLayout::new();
    /// super_layout.move_up();
    /// ```
    pub fn move_up(&mut self) {
        if self.selectable.len() >= 1 {
            if let Some(&i) = self.selectable.iter().rev().find(|&&i| i < self.focus) {
                self.focus = i;
            } else if self.topmost {
                self.focus = *self.selectable.last().unwrap();
            }
        }
    }

    fn handle_left(&mut self, keypress: KeyEvent) -> Option<KeyEvent> {
        if self.direction == Direction::Horizontal {
            let before = self.focus;
            self.move_up();
            if self.focus != before {
                return None;
            };
        };
        Some(keypress)
    }

    fn handle_up(&mut self, keypress: KeyEvent) -> Option<KeyEvent> {
        if self.direction == Direction::Vertical {
            let before = self.focus;
            self.move_up();
            if self.focus != before {
                return None;
            };
        };
        Some(keypress)
    }

    /// Move the pointer 1 down, moves to the first item if the last item is selected
    ///
    /// ```
    /// use crate::ui::widgets::SuperLayout;
    /// let mut super_layout: SuperLayout = SuperLayout::new();
    /// super_layout.move_down();
    /// ```
    pub fn move_down(&mut self) {
        if self.selectable.len() >= 1 {
            if let Some(&i) = self.selectable.iter().find(|&&i| i > self.focus) {
                self.focus = i;
            } else if self.topmost {
                self.focus = *self.selectable.first().unwrap();
            }
        }
    }

    fn handle_right(&mut self, keypress: KeyEvent) -> Option<KeyEvent> {
        if self.direction == Direction::Horizontal {
            let before = self.focus;
            self.move_down();
            if self.focus != before {
                return None;
            };
        };
        Some(keypress)
    }

    fn handle_down(&mut self, keypress: KeyEvent) -> Option<KeyEvent> {
        if self.direction == Direction::Vertical {
            let before = self.focus;
            self.move_down();
            if self.focus != before {
                return None;
            };
        };
        Some(keypress)
    }

    fn handle_tab(&mut self, keypress: KeyEvent) -> Option<KeyEvent> {
        let before = self.focus;
        self.move_down();
        if self.focus != before {
            return None;
        };
        Some(keypress)
    }

    pub fn move_set_topmost(mut self) -> SuperLayout {
        self.topmost = true;
        self
    }

    fn get_chunks(&self, area: Rect) -> Vec<Rect> {
        Layout::default()
            .direction(self.direction.clone())
            .constraints(self.constraints.clone())
            .split(area)
    }
}

impl SuperWidget for SuperLayout {
    fn render(&mut self, frame: &mut Frame<CrosstermBackend<Stdout>>, area: Rect) {
        if self.members.len() > self.constraints.len() {
            panic!("SuperLayout has more members than constraints")
        }
        let chunks = self.get_chunks(area);
        for i in 0..self.members.len() {
            self.members[i].render(frame, chunks[i]);
        }
    }

    fn cursor_position(&self, area: Rect) -> Rect {
        let chunks = self.get_chunks(area);
        if self.members.len() > 0 {
            self.members[self.focus].cursor_position(chunks[self.focus])
        } else {
            area
        }
    }

    fn selected(&self) -> &dyn SuperWidget {
        self.members[self.focus].as_ref()
    }
    fn mut_selected(&mut self) -> &mut dyn SuperWidget {
        self.members[self.focus].as_mut()
    }
    fn keypress(&mut self, keypress: KeyEvent) -> Option<KeyEvent> {
        let new_keypress = self.mut_selected().keypress(keypress)?;
        match new_keypress.code {
            KeyCode::Up => self.handle_up(new_keypress),
            KeyCode::Down => self.handle_down(new_keypress),
            KeyCode::Left => self.handle_left(new_keypress),
            KeyCode::Right => self.handle_right(new_keypress),
            KeyCode::Tab => self.handle_tab(new_keypress),
            _ => Some(new_keypress),
        }
    }

    fn selectable(&self) -> bool {
        self.members.iter().any(|i| i.as_ref().selectable())
    }
}
