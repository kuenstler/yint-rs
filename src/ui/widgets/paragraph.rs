use std::io::Stdout;

use crossterm::event::KeyEvent;
use tui::{
    backend::CrosstermBackend, layout::Rect, style::Style, text::Spans, widgets::Paragraph, Frame,
};

use crate::ui::widgets::SuperWidget;

pub struct SuperParagraph {
    pub text: String,
    pub style: Style,
}

impl SuperParagraph {
    pub fn new(text: String) -> SuperParagraph {
        SuperParagraph {
            text,
            style: Default::default(),
        }
    }

    pub fn move_set_text(mut self, text: String) -> SuperParagraph {
        self.text = text;
        self
    }

    pub fn move_set_style(mut self, style: Style) -> SuperParagraph {
        self.style = style;
        self
    }
}

impl SuperWidget for SuperParagraph {
    fn render(&mut self, frame: &mut Frame<CrosstermBackend<Stdout>>, area: Rect) {
        let paragraph = Paragraph::new(Spans::from(self.text.clone())).style(self.style);
        frame.render_widget(paragraph, area);
    }

    fn cursor_position(&self, area: Rect) -> Rect {
        area
    }

    fn selected(&self) -> &dyn SuperWidget {
        self
    }

    fn mut_selected(&mut self) -> &mut dyn SuperWidget {
        self
    }

    fn keypress(&mut self, keypress: KeyEvent) -> Option<KeyEvent> {
        Some(keypress)
    }

    fn selectable(&self) -> bool {
        false
    }
}
