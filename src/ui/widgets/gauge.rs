use std::io::Stdout;

use tui::{
    backend::CrosstermBackend,
    layout::{Constraint, Direction, Layout, Rect},
    style::{Color, Style},
    widgets::{Gauge, Paragraph, Wrap},
    Frame,
};

use crate::ui::widgets::{Error, KeyEvent, SuperWidget};

/// Gauge with tag
/// Doesn't get consumed while rendering
///
/// # Note
///
/// If you have a tag, the widget needs to be 2 lines high
///
/// # Examples
///
/// ```
/// use crate::ui::widgets::SuperGauge;
/// let tag = "Hello World".to_string();
/// let mut super_gauge: SuperGauge = SuperGauge::new()
///     .move_set_ratio(0.5).unwrap();
/// super_gauge.set_tag(tag.clone());
///
/// assert_eq!(super_gauge.ratio(), 0.5);
/// assert_eq!(super_gauge.tag(), tag);
/// ```
pub struct SuperGauge {
    ratio: f64,
    pub tag: String,
    pub style: Style,
    pub gauge_style: Style,
    pub additional: String,
}

impl SuperGauge {
    /// Construct a new SuperGauge with default arguments
    pub fn new() -> SuperGauge {
        SuperGauge {
            tag: "".to_string(),
            ratio: 0.0,
            style: Style::default(),
            gauge_style: Style::default().fg(Color::White).bg(Color::Rgb(0, 0, 0)),
            additional: "".to_string(),
        }
    }

    /// Set the ratio ot a specific number within, and including, 0 and 1.
    /// Returns an error if the ratio is out of range
    ///
    /// # Examples
    ///
    /// ```
    /// use crate::ui::widgets::SuperGauge;
    /// let mut super_gauge: SuperGauge = SuperGauge::new();
    /// super_gauge.set_ratio(0.5).unwrap();
    ///
    /// assert_eq!(super_gauge.ratio(), 0.5);
    /// ```
    pub fn set_ratio(&mut self, ratio: f64) -> Result<(), Error> {
        if ratio < 0.0 || ratio > 1.0 {
            return Err(Error {
                message: "Unexpected value for Progress".to_string(),
            });
        }
        self.ratio = ratio;
        Ok(())
    }

    /// Set the ratio ot a specific number within, and including, 0 and 1.
    /// Returns an error if the ratio is out of range or the Gauge itself upon success
    ///
    /// # Examples
    ///
    /// ```
    /// use crate::ui::widgets::SuperGauge;
    /// let super_gauge: SuperGauge = SuperGauge::new()
    ///     .move_set_ratio(0.5).unwrap();
    ///
    /// assert_eq!(super_gauge.ratio(), 0.5);
    /// ```
    pub fn move_set_ratio(mut self, ratio: f64) -> Result<SuperGauge, Error> {
        self.set_ratio(ratio)?;
        Ok(self)
    }

    pub fn ratio(&self) -> f64 {
        self.ratio
    }

    pub fn move_set_tag(mut self, tag: String) -> SuperGauge {
        self.tag = tag;
        self
    }

    pub fn move_set_style(mut self, style: Style) -> SuperGauge {
        self.style = style;
        self
    }

    pub fn move_set_gauge_style(mut self, style: Style) -> SuperGauge {
        self.gauge_style = style;
        self
    }
}

impl SuperWidget for SuperGauge {
    fn render(&mut self, frame: &mut Frame<CrosstermBackend<Stdout>>, area: Rect) {
        let label = if self.additional == "".to_string() {
            format!("{:.1}%", self.ratio * 100.0)
        } else {
            format!("{:.1}% from {}", self.ratio * 100.0, self.additional)
        };
        let gauge = Gauge::default()
            .ratio(self.ratio)
            .gauge_style(self.gauge_style)
            .label(label);
        if self.tag == "".to_string() {
            frame.render_widget(gauge, area);
        } else {
            let chunks = Layout::default()
                .constraints([Constraint::Length(1), Constraint::Min(1)].as_ref())
                .direction(Direction::Vertical)
                .split(area);
            let paragraph = Paragraph::new(&self.tag[..]).wrap(Wrap { trim: true });
            frame.render_widget(paragraph, chunks[0]);
            frame.render_widget(gauge, chunks[1]);
        }
    }

    fn cursor_position(&self, area: Rect) -> Rect {
        area
    }

    fn selected(&self) -> &dyn SuperWidget {
        self
    }

    fn mut_selected(&mut self) -> &mut dyn SuperWidget {
        self
    }

    fn keypress(&mut self, keypress: KeyEvent) -> Option<KeyEvent> {
        Some(keypress)
    }

    fn selectable(&self) -> bool {
        false
    }
}
