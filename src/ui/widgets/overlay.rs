use std::io::Stdout;

use crossterm::event::KeyEvent;
use tui::{
    backend::CrosstermBackend,
    layout::{Constraint, Direction, Layout, Rect},
    style::{Color, Style},
    widgets::{Block, Borders, Paragraph, Wrap},
    Frame,
};

use crate::ui::widgets::SuperWidget;

pub struct SuperOverlay {
    pub widget: Box<dyn SuperWidget>,
    pub margin: u16,
    pub borders: bool,
    pub title: String,
    last_area: Rect,
}

impl SuperOverlay {
    pub fn new(widget: Box<dyn SuperWidget>) -> SuperOverlay {
        SuperOverlay {
            widget,
            margin: 20,
            borders: true,
            title: "".to_string(),
            last_area: Rect::default(),
        }
    }

    fn get_working_area(&self) -> Rect {
        let outer_layout = Layout::default()
            .constraints(vec![
                Constraint::Percentage(self.margin),
                Constraint::Percentage(60),
                Constraint::Percentage(self.margin),
            ])
            .direction(Direction::Horizontal)
            .split(self.last_area);
        Layout::default()
            .constraints(vec![
                Constraint::Percentage(self.margin),
                Constraint::Percentage(60),
                Constraint::Percentage(self.margin),
            ])
            .direction(Direction::Vertical)
            .split(outer_layout[1])[1]
    }

    fn get_block(&self) -> Block {
        let mut block = Block::default().borders(Borders::ALL);
        if self.title != "".to_string() {
            block = block.title(&self.title[..])
        };
        block
    }
}

impl SuperWidget for SuperOverlay {
    fn render(&mut self, frame: &mut Frame<CrosstermBackend<Stdout>>, _area: Rect) {
        // Get the size to work on
        self.last_area = frame.size();
        let mut area = self.get_working_area();
        // If borders are selected, draw them and remove them from area
        if self.borders {
            let block = self.get_block();
            let new_area = block.inner(area);
            frame.render_widget(block, area);
            area = new_area;
        }
        // Clean the inner region
        let length = (area.width - area.x) * (area.height - area.y);
        let long_string = " ".repeat(length as usize);
        let paragraph = Paragraph::new(&long_string[..])
            .style(Style::default().fg(Color::White).bg(Color::Rgb(0, 0, 0)))
            .wrap(Wrap { trim: false });
        frame.render_widget(paragraph, area);
        // Render the child widget
        self.mut_selected().render(frame, area);
    }

    fn cursor_position(&self, _area: Rect) -> Rect {
        let working_area = self.get_working_area();
        self.selected().cursor_position(if self.borders {
            let block = self.get_block();
            block.inner(working_area)
        } else {
            working_area
        })
    }

    fn selected(&self) -> &dyn SuperWidget {
        self.widget.as_ref()
    }

    fn mut_selected(&mut self) -> &mut dyn SuperWidget {
        self.widget.as_mut()
    }

    fn keypress(&mut self, keypress: KeyEvent) -> Option<KeyEvent> {
        self.mut_selected().keypress(keypress)
    }

    fn selectable(&self) -> bool {
        self.selected().selectable()
    }
}
