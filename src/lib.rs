mod utilities;

/// The application's ui
pub mod ui;

mod helpers;

pub fn run() {
    ui::main().unwrap();
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 * 2, 4);
    }
}
